import java.util.Scanner;

public class Homework07 {

    public static int getIndex(int[] array, int numbF) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == numbF) {
                return i;
            }
        }
        return (-1);
    }

    public static void getNewArr(int[] array) {
        int forChange = 0;
        int i = 0;
        while (i < array.length) {
            if (array[i] == 0) {
                forChange = i;
                while ((forChange + 1 < array.length) && (array[forChange + 1] == 0)) {
                    forChange++;
                }
                if (forChange + 1 < array.length) {
                    array[i] = array[forChange + 1];
                    array[forChange + 1] = 0;
                }
            }
            i++;
        }
    }

    public static void  print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = new int[201];
        int minVal = 2147483647;
        int target = 99;
        /**
         *  Результат -1 (99-100) покажет, что данные не вводились
         */

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter integer in range [-100;100]: ");
        int numb = scanner.nextInt();
        while (numb < -100 || numb > 100) {
            System.out.print("Attention! Enter integer in range [-100;100] !!!!: ");
            numb = scanner.nextInt();
        }

        while (numb != -1) {
            arr[numb + 100]++;
            System.out.print("Enter integer in range [-100;100]: ");
            numb = scanner.nextInt();
            while (numb < -100 || numb > 100) {
                System.out.print("Attention! Enter integer in range [-100;100] !!!!: ");
                numb = scanner.nextInt();
            }
        }

        for (int i = 0; i < 201; i++) {
            if (arr[i] <= minVal && arr[i] != 0) {
                minVal = arr[i];
                target = i;
            }
        }

        if (target - 100 == -1)
            System.out.println("Result: no entry data");
        else
            System.out.println("Result: " + (target - 100));
    }
}
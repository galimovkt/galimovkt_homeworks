package Homework21;

import Homework21.Homework21;

public class SumThread extends Thread {
    private int from;
    private int to;
    private int[] array;
    private int sum;

    public SumThread(int from, int to, int[] array) {
        this.from = from;
        this.to = to;
        this.array = array;
        this.sum = 0;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            this.sum += array[i];
        }
    }

}

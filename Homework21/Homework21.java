package Homework21;

import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Homework21 {
    public static int[] array;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        System.out.println(realSum);

        // реализовать работу с потоками

        ArrayList<SumThread> list = new ArrayList<>();
        list.add(new SumThread(0, 4, array));
        list.add(new SumThread(5, 9, array));
        list.add(new SumThread(10, 14, array));
        list.add(new SumThread(15, 16, array));


        for (SumThread i : list) {
            i.start();
        }

        for (SumThread i : list) {
            try {
                i.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }

        int byThreadSum = 0;

        for (SumThread i : list) {
            byThreadSum += i.getSum();
        }

        System.out.println(byThreadSum);
    }
}


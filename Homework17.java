import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

public class Homework17 {
    private static final String inputString = "Здравствуйте уважаемый сударь любезный! Не соизволите ли проверить данную работу " +
            "сударь может проверить работу за один день. Внимательно проверить за два дня.";


    public static void main(String[] args) {
        String[] words = inputString.split(" ");

        // 1 способ
        Map<String, Integer> dictionary1 = new HashMap<>();
        for (String word : words) {
            if (dictionary1.containsKey(word)) {
                dictionary1.replace(word, dictionary1.get(word) + 1);
            } else {
                dictionary1.put(word, 1);
            }
        }

        // 2 способ
        Map<String, Integer> dictionary2 = new HashMap<>();
        for (String word : words) {
            dictionary2.compute(word, (key, value) -> ((value == null) ? 1 : value + 1));
        }

        System.out.println(Arrays.toString(words));
        System.out.println(dictionary1);
        System.out.println(dictionary2);

    }
}
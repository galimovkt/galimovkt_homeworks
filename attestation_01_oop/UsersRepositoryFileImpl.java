package attestation_01_oop;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            // прочитали строку
            String line = reader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                String[] parts = line.split("\\|");

                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);

                // создаем нового человека
                User newUser = new User(id, name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = new ArrayList<>();
        for (User user : findAll()) {
            int id = user.getId();
            String name = user.getName();
            int age = user.getAge();
            boolean isWorker = user.isWorker();
            if (isWorker) {
                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);
            }
        }
        return users;
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<>();
        for (User user : this.findAll()) {
            int id = user.getId();
            String name = user.getName();
            int ageOfPerson = user.getAge();
            boolean isWorker = user.isWorker();
            if (ageOfPerson == age) {
                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);
            }
        }
        return users;
    }

    @Override
    public User findById(int id) {
        for (User user : this.findAll()) {
            int idOfPerson = user.getId();
            if (idOfPerson == id) {
                String name = user.getName();
                int ageOfPerson = user.getAge();
                boolean isWorker = user.isWorker();
                return (new User(id, name, ageOfPerson, isWorker));
            }
        }
        System.out.println("User с id = " + id + " не найден!");
        // нужно вернуть пустой юзер
        return (new User(0, "", 0, false));
    }

    @Override
    public void update(User user) {
        List<User> usersAll = this.findAll();
        try {
            Files.deleteIfExists(Paths.get("users.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        UsersRepository usersRepository1 = new UsersRepositoryFileImpl("users.txt");

        for (User userIn : usersAll) {
            if (userIn.getId() == user.getId()) {
                userIn.setName(user.getName());
                userIn.setAge(user.getAge());
                userIn.setWorker(user.isWorker());
            }
            usersRepository1.save(userIn);
        }
    }
}
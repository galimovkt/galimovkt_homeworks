package attestation_01_oop;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Attestation_01_oop {

    public static void main(String[] args) {
        try {
            Files.deleteIfExists(Paths.get("users.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        User userNew = new User(1, "Игорь", 33, true);
        usersRepository.save(userNew);
        userNew = new User(2, "Джон", 30, false);
        usersRepository.save(userNew);
        userNew = new User(3, "Василий", 30, true);
        usersRepository.save(userNew);
        userNew = new User(4, "Ренат", 30, true);
        usersRepository.save(userNew);
        userNew = new User(5, "Билл", 25, false);
        usersRepository.save(userNew);


        List<User> users = usersRepository.findAll();
        System.out.println(">>> findAll");
        for (User user : users) {
            System.out.println(user.getId() + " " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        User findUser = usersRepository.findById(1);
        findUser.setName("Марсель");
        findUser.setAge(27);
        usersRepository.update(findUser);

        System.out.println(">>> findByAge");
        List<User> users1 = usersRepository.findByAge(30);
        for (User user : users1) {
            System.out.println(user.getId() + " " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        System.out.println(">>> findByIsWorkerIsTrue");
        List<User> users2 = usersRepository.findByIsWorkerIsTrue();
        for (User user : users2) {
            System.out.println(user.getId() + " " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }

}
import java.util.ArrayList;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        ArrayList<Integer> arr = new ArrayList<>();
        for (int elementOfArray : array) {
            if (condition.isOk(elementOfArray))
                arr.add(elementOfArray);
        }
        return arr.stream().mapToInt(x -> x).toArray();
    }
}

import java.util.Arrays;

public class Homework13 {

    public static int sumNumb(int number) {
        int s = 0;
        while (number != 0 ) {
            s += number % 10;
            number /= 10;
        }
        return s;
    }

    public static void main(String[] args) {
        int[] array = {25, 27, 536, 122, 227, 24};
        System.out.println(Arrays.toString(array));

        int[] a = Sequence.filter(array, number -> (number % 2 == 0 && sumNumb(number) % 2 == 0));
        System.out.println(Arrays.toString(a));

    }
}


public class Logger {

    private static final Logger instance;

    static {
        instance = new Logger();
    }
    
    public static Logger getInstance() {
        return instance;
    }

    void log(String message) {
        System.out.println(message);
    }
}
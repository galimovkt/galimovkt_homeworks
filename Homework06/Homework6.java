import java.util.Scanner;

class Homework6 {

    public static int getIndex(int[] array, int numbF) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == numbF) {
                return i;
            }
        }
        return (-1);
    }

    public static void getNewArr(int[] array) {
        int forChange = 0;
        int i = 0;
        while (i < array.length) {
            if (array[i] == 0) {
                forChange = i;
                while ((forChange + 1 < array.length) && (array[forChange + 1] == 0)) {
                    forChange++;
                }
                if (forChange + 1 < array.length) {
                    array[i] = array[forChange + 1];
                    array[forChange + 1] = 0;
                }
            }
            i++;
        }
    }

    public static void  print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enetr integer: ");
        int numb = scanner.nextInt();

        int targetIndex = getIndex(arr, numb);
        System.out.println("Target index: " + targetIndex);

        System.out.print("Old array: ");
        print(arr);
        getNewArr(arr);
        System.out.print("New array: ");
        print(arr);


    }
}
public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        Node<T> currentNode = first;
        int i = 0;

        if (index >= 0 && index < size) {
            while (currentNode.next != null) {
                if (i == index) {
                    return (currentNode.value);
                } else
                    currentNode = currentNode.next;
                i++;
            }
        } else
            System.out.println("Index out of size");
        return null;
    }

    public String stringLinkedList() {
        Node<T> currentNode = first;
        StringBuilder builder = new StringBuilder();
        while (currentNode.next != null) {
            if (currentNode != first) {
                builder.append(", ");
            }
            builder.append(currentNode.value);
            currentNode = currentNode.next;
        }
        String str = builder.toString();
        return str;
    }

    @Override
    public String toString() {
        return "LinkedList: size= " + size + " LinkedList= [" + stringLinkedList() + "]";
    }


}

public class Homework16 {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);

        System.out.println(numbers);
        numbers.removeAt(8);
        System.out.println(numbers);
        numbers.removeAt(-5);
        System.out.println(numbers);
        numbers.removeAt(3);
        System.out.println(numbers);
        numbers.removeAt(0);
        System.out.println(numbers);


        LinkedList<Integer> list = new LinkedList<>();
        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(99);

        list.addToBegin(77);
        list.addToBegin(88);
        list.addToBegin(99);

        System.out.println("=== LinkedList ===");
        System.out.println(list);
        System.out.println("Value of list[0]= " + list.get(0));
        System.out.println("Value of list[3]= " + list.get(3));
        System.out.println("Value of list[5]= " + list.get(5));

    }
}

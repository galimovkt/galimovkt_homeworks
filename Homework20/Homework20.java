package Homework20;

import java.lang.Integer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.*;

public class Homework20 {

    public static void main(String[] args) throws IOException {

        final BufferedReader reader = read("input.txt");
        final ArrayList<String> arrayList = reader.lines().collect(Collectors.toCollection(ArrayList::new));
        reader.close();

        do_1(arrayList);
        do_2(arrayList);
        do_3(arrayList);
        do_4(arrayList);
    }

    public static void do_1(ArrayList<String> arrayList) {
        System.out.println(">>> 1: All numbers of cars with black color or distance = 0");

        arrayList.stream()
                .map(line -> line.split("\\|"))
                .filter(a -> (a[2].equals("Black") || a[3].equals("0")))
                .map(a -> a[0])
                .forEach(line -> System.out.println(line));
    }

    public static void do_2(ArrayList<String> arrayList) {
        System.out.println(">>> 2 Count unique models with price >= 7000000 and <= 800000");

        long countWithPrice = arrayList.stream()
                .map(line -> line.split("\\|"))
                .filter(a -> (Integer.parseInt(a[4]) >= 700000) && (Integer.parseInt(a[4]) <= 800000))
                .map(a -> a[1])
                .distinct()
                .count();
        System.out.println(countWithPrice);
    }

    public static void do_3(ArrayList<String> arrayList) {
        System.out.println(">>> 3: Minimum price - color, all cars colors with minimum price");

        int min = arrayList.stream()
                .map(line -> line.split("\\|"))
                .mapToInt(a -> Integer.parseInt(a[4]))
                .min()
                .getAsInt();

        System.out.println("Min = " + min);

        arrayList.stream()
                .map(line -> line.split("\\|"))
                .filter(a -> (Integer.parseInt(a[4]) == min))
                .map(a -> a[2])
                .forEach(line -> System.out.println(line));
    }

    public static void do_4(ArrayList<String> arrayList)  {
        System.out.println(">>> 4: Average price of Camry");

        double camryAveragePrice = arrayList.stream()
                .map(line -> line.split("\\|"))
                .filter(a -> (a[1].equals("Camry")))
                .mapToInt(a -> Integer.parseInt(a[4]))
                .average()
                .getAsDouble();
        System.out.println(camryAveragePrice);
    }


    public static BufferedReader read(final String filePath) throws IOException {
        try {
            return new BufferedReader(new FileReader(filePath));
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }
}

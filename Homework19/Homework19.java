package Homework19;

import java.util.List;

public class Homework19 {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        User userNew = new User("Игорь", 33, true);
        usersRepository.save(userNew);
        userNew = new User("Джон", 30, false);
        usersRepository.save(userNew);
        userNew = new User("Василий", 30, true);
        usersRepository.save(userNew);
        userNew = new User("Ренат", 30, true);
        usersRepository.save(userNew);
        userNew = new User("Билл", 25, false);
        usersRepository.save(userNew);


        List<User> users = usersRepository.findAll();
        System.out.println(">>> findAll");
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        System.out.println(">>> findByAge");
        List<User> users1 = usersRepository.findByAge(30);
        for (User user : users1) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        System.out.println(">>> findByIsWorkerIsTrue");
        List<User> users2 = usersRepository.findByIsWorkerIsTrue();
        for (User user : users2) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }
}

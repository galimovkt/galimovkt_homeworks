package Homework19;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            // прочитали строку
            String line = reader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем имя
                String name = parts[0];
                // берем возраст
                int age = Integer.parseInt(parts[1]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                // создаем нового человека
                User newUser = new User(name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = new ArrayList<>();
        for (User user : findAll()) {
            String name = user.getName();
            int age = user.getAge();
            boolean isWorker = user.isWorker();
            if (isWorker) {
                User newUser = new User(name, age, isWorker);
                users.add(newUser);
            }
        }
        return users;
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<>();
        for (User user : findAll()) {
            String name = user.getName();
            int ageOfPerson = user.getAge();
            boolean isWorker = user.isWorker();
            if (ageOfPerson == age) {
                User newUser = new User(name, age, isWorker);
                users.add(newUser);
            }
        }
        return users;
    }
}



package Homework19;

public class User {
    private String name;
    private int age;
    private boolean isWorker;

    public User(String name, int age, boolean isWorker) {
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }
}

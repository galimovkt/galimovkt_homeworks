import java.util.Scanner;

public class Homework08 {

    public static void printArr(Human[] array) {
        for (int i = 0; i < 10; i++) {
            System.out.println((i + 1) + ". name: " + array[i].getName() + ", weight: " + (array[i].getWeight()));
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String rezName;
        Human[] arrHuman = new Human[10];
        for (int i = 0; i < 10 ; i++) {
            arrHuman[i] = new Human();
        }

        for (int i = 0; i < 10; i++) {
            System.out.print("Enter NAME of person N " + (i + 1) + ": ");
            arrHuman[i].setName(scanner.next());
            System.out.print("Enter WEIGHT (kg) of person N " + (i + 1) + ": ");
            arrHuman[i].setWeight(scanner.nextDouble());
        }


        for (int i = 0; i < 9; i++) {
            double minWeight = arrHuman[i].getWeight() ;
            int minIndex = i;
            for (int j = i + 1; j < 10 ; j++) {
                if (arrHuman[j].getWeight() < minWeight) {
                    minWeight = arrHuman[j].getWeight();
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                rezName = arrHuman[i].getName();
                arrHuman[i].setName(arrHuman[minIndex].getName());
                arrHuman[minIndex].setName(rezName);

                arrHuman[minIndex].setWeight(arrHuman[i].getWeight());
                arrHuman[i].setWeight(minWeight);
            }
        }

        printArr(arrHuman);

    }
}
public class Square extends Rectangle implements Transfer {
    public Square(double x, double y, double side) {
        super(x, y, side, side); // вызов конструктора предка
        System.out.println(this);
    }

    @Override
    public void getTransfer(double newX, double newY) {
        this.setX(newX);
        this.setY(newY);
    }

    @Override
    public String toString() {
        return super.toString() + ", side = " + getSideA();
    }
}

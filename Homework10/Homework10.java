public class Homework10 {
    public static void main(String[] args) {

        Transfer[] transfers = new Transfer[4];
        transfers[0] = new Circle(50, 50, 20);
        transfers[1] = new Circle(50, -50, 15);
        transfers[2] = new Square(-50, 50, 10);
        transfers[3] = new Square(-50, -50, 5);

        int targetX = 0;
        int targetY = 0;
        System.out.println("New coordinates x = " + targetX + ", y = " + targetY);

        for (int i = 0; i < transfers.length; i++) {
            transfers[i].getTransfer(targetX, targetY);
            System.out.println(transfers[i]);
        }
    }
}

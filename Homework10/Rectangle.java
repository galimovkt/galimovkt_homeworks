public class Rectangle extends Figure {
    private double sideA;
    private double sideB;

    public Rectangle(double x, double y, double sideA, double sideB) {
        super(x, y);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getSideA() {
        return this.sideA;
    }
}


public class Circle extends Ellipse implements Transfer {

    public Circle(double x, double y, double radius) {
        super(x, y, radius, radius);
        System.out.println(this);
    }

    @Override
    public void getTransfer(double newX, double newY) {
        this.setX(newX);
        this.setY(newY);
    }

    @Override
    public String toString() {
        return super.toString() + ", radius = " + getRadius1();
    }
}

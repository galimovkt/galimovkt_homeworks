public class Ellipse extends Figure {
    private double radius1;
    private double radius2;

    public Ellipse(double x, double y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getRadius1() {
        return this.radius1;
    }

}

package Homework25;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private int id;
    private String describe;
    private double price;
    private int amount;
}
package Homework25;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Homework25 {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/schema", "kamil", "125125");

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        System.out.println(productsRepository.findAll());

        System.out.println(productsRepository.findAllByPrice(1999.99));

        System.out.println(productsRepository.findAllByOrdersCount(3));

    }
}



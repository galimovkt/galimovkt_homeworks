package Homework25;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    // language=SQL
    public static final String SQL_INSERT = "insert into goods(describe, price, amount) values (?, ?, ?)";

    // language=SQL
    public static final String SQL_SELECT_ALL = "select * from goods order by id";


    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public static final RowMapper<Product> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String describe = row.getString("describe");
        double price = row.getDouble("price");
        int amount = row.getInt("amount");

        return new Product(id, describe, price, amount);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        // language=SQL
        final String SQL_SELECT_PRICE = "select * from goods where price = " + price + " order by id";

        return jdbcTemplate.query(SQL_SELECT_PRICE, userRowMapper);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        // language=SQL
        final String SQL_SELECT_PRICE = "select * from goods where goods.id in (select goods_id from ord where amount_of_goods = " + ordersCount + ")";

        return jdbcTemplate.query(SQL_SELECT_PRICE, userRowMapper);
    }

}


create table goods
(
    id       serial primary key,
    describe varchar(100),
    price    money,
    amount   integer
)

create table customer
(
    id              serial primary key,
    first_last_name varchar(40)
)

create table ord
(
    id              serial primary key,
    customer_id     integer,
    foreign key (customer_id) references customer (id),
    goods_id        integer,
    foreign key (goods_id) references goods (id),
    date_of_order   date,
    amount_of_goods integer
)

insert into goods(id, describe, price, amount) values (1, '�������', 1999.99, 100);
insert into goods(id, describe, price, amount) values (2, '������', 4500, 50);
insert into goods(id, describe, price, amount) values (3, '���������', 350.25, 100);
insert into goods(id, describe, price, amount) values (4, '������������', 900, 50);
insert into goods(id, describe, price, amount) values (5, '���������', 2500, 20);

insert into customer(id, first_last_name) values (1, '������ ���������');
insert into customer(id, first_last_name) values (2, '������� ���������');
insert into customer(id, first_last_name) values (3, '�������� ������');
insert into customer(id, first_last_name) values (4, '������ ��������');
insert into customer(id, first_last_name) values (5, '����� ������');

insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (1, 1, 1, '10/12/2021', 3);
insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (2, 1, 3, '10/12/2021', 3);
insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (3, 2, 2, '10/15/2021', 1);
insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (4, 2, 4, '10/15/2021', 1);
insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (5, 3, 5, '10/17/2021', 2);
insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (6, 4, 2, '10/15/2021', 10);
insert into ord(id, customer_id, goods_id, date_of_order, amount_of_goods) values (7, 5, 1, '10/20/2021', 5);

select * from goods;

select count(*) from goods;

select count(*) from goods where amount > 50;

select * from customer;

select count(*) from customer;

select first_last_name from customer order by first_last_name;

select * from customer order by first_last_name desc;

select * from ord;

select count(*) from ord;

select distinct(customer_id) from ord;

select first_last_name, (select count(*) from ord where customer_id = customer.id) as purchase_count
from customer;

select first_last_name
from customer
where customer.id in
      (select customer_id from ord where goods_id in (select goods.id from goods where describe = '�������'));


select a.first_last_name, d.describe,  b.date_of_order, b.amount_of_goods, d.price as price_for_one, (d.price * b.amount_of_goods) as price_for_all
from ord b left join customer a on a.id = b.customer_id left join goods d on b.goods_id = d.id;

select first_last_name, sum(new.amount_of_goods), sum(price_for_all)
from (select a.first_last_name, b.amount_of_goods, (d.price * b.amount_of_goods) as price_for_all from ord b left join customer a on a.id = b.customer_id left join goods d on b.goods_id = d.id) as new
group by new.first_last_name
order by new.first_last_name;

select new.describe, sum(new.amount_of_goods) as count, sum(price_for_all)
from (select d.describe, b.amount_of_goods, (d.price * b.amount_of_goods) as price_for_all from ord b left join customer a on a.id = b.customer_id left join goods d on b.goods_id = d.id) as new
group by new.describe
order by new.describe;
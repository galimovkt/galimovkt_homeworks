--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: kamil
--

CREATE TABLE public.customer (
    id integer NOT NULL,
    first_last_name character varying(40)
);


ALTER TABLE public.customer OWNER TO kamil;

--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: kamil
--

CREATE SEQUENCE public.customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_id_seq OWNER TO kamil;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kamil
--

ALTER SEQUENCE public.customer_id_seq OWNED BY public.customer.id;


--
-- Name: goods; Type: TABLE; Schema: public; Owner: kamil
--

CREATE TABLE public.goods (
    id integer NOT NULL,
    describe character varying(100),
    price money,
    amount integer
);


ALTER TABLE public.goods OWNER TO kamil;

--
-- Name: goods_id_seq; Type: SEQUENCE; Schema: public; Owner: kamil
--

CREATE SEQUENCE public.goods_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_id_seq OWNER TO kamil;

--
-- Name: goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kamil
--

ALTER SEQUENCE public.goods_id_seq OWNED BY public.goods.id;


--
-- Name: ord; Type: TABLE; Schema: public; Owner: kamil
--

CREATE TABLE public.ord (
    id integer NOT NULL,
    customer_id integer,
    goods_id integer,
    date_of_order date,
    amount_of_goods integer
);


ALTER TABLE public.ord OWNER TO kamil;

--
-- Name: ord_id_seq; Type: SEQUENCE; Schema: public; Owner: kamil
--

CREATE SEQUENCE public.ord_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ord_id_seq OWNER TO kamil;

--
-- Name: ord_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kamil
--

ALTER SEQUENCE public.ord_id_seq OWNED BY public.ord.id;


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.customer ALTER COLUMN id SET DEFAULT nextval('public.customer_id_seq'::regclass);


--
-- Name: goods id; Type: DEFAULT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.goods ALTER COLUMN id SET DEFAULT nextval('public.goods_id_seq'::regclass);


--
-- Name: ord id; Type: DEFAULT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.ord ALTER COLUMN id SET DEFAULT nextval('public.ord_id_seq'::regclass);


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: kamil
--

COPY public.customer (id, first_last_name) FROM stdin;
1	Петров Александр
2	Радулов Александр
3	Фернандо Алонсо
4	Сергей Родионов
5	Ринат Дасаев
\.


--
-- Data for Name: goods; Type: TABLE DATA; Schema: public; Owner: kamil
--

COPY public.goods (id, describe, price, amount) FROM stdin;
1	Подушка	$1,999.99	100
2	Одеяло	$4,500.00	50
3	Наволочка	$350.25	100
4	Пододеяльник	$900.00	50
5	Покрывало	$2,500.00	20
\.


--
-- Data for Name: ord; Type: TABLE DATA; Schema: public; Owner: kamil
--

COPY public.ord (id, customer_id, goods_id, date_of_order, amount_of_goods) FROM stdin;
1	1	1	2021-10-12	3
2	1	3	2021-10-12	3
3	2	2	2021-10-15	1
4	2	4	2021-10-15	1
5	3	5	2021-10-17	2
6	4	2	2021-10-15	10
7	5	1	2021-10-20	5
\.


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kamil
--

SELECT pg_catalog.setval('public.customer_id_seq', 1, false);


--
-- Name: goods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kamil
--

SELECT pg_catalog.setval('public.goods_id_seq', 1, false);


--
-- Name: ord_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kamil
--

SELECT pg_catalog.setval('public.ord_id_seq', 1, false);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: goods goods_pkey; Type: CONSTRAINT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.goods
    ADD CONSTRAINT goods_pkey PRIMARY KEY (id);


--
-- Name: ord ord_pkey; Type: CONSTRAINT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.ord
    ADD CONSTRAINT ord_pkey PRIMARY KEY (id);


--
-- Name: ord ord_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.ord
    ADD CONSTRAINT ord_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customer(id);


--
-- Name: ord ord_goods_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kamil
--

ALTER TABLE ONLY public.ord
    ADD CONSTRAINT ord_goods_id_fkey FOREIGN KEY (goods_id) REFERENCES public.goods(id);


--
-- PostgreSQL database dump complete
--

